#
###############################################################################
#
#
# start initialization after iocBoot()

# comment out if autosave is not used
< autosave.init
# comment out if acquisition is not used
< acq.init

# end initialization after iocBoot()
#
# IOC running!
#
