########################################
#        IOC startup entry point       #
########################################

# instance.cmd defines all variables that differe between instances of this
# IOC. For example hardware IDs, Naming service names,..
< instance.cmd

# get paths to EPICS base and modules
< envPaths

errlogInit(20000)

dbLoadDatabase("$(IOCAPP)/dbd/iocApp.dbd")
iocApp_registerRecordDeviceDriver(pdbbase)

# PATH: we need caput and caRepeater
epicsEnvSet("PATH",                         "$(PATH):$(EPICS_BASE)/bin/linux-x86_64")
# EPICS_CA_MAX_ARRAY_BYTES: 10 MB max CA request
# epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")
# EPICS_DB_INCLUDE_PATH: list all module db folders
epicsEnvSet("EPICS_DB_INCLUDE_PATH",        "$(TOP)/db:$(ASYN)/db:$(AUTOSAVE)/db:$(TLMFF101)/db:$(TLMFF101APP)/db")

< setup.cmd

iocInit()

< init.cmd
